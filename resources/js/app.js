require('./bootstrap');

window.Vue = require('vue');

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';

Vue.use(VueRouter,VueAxios,Axios);

// import file yang dibuat tadi
import App from './components/App.vue'
import Create from './components/admin/Create.vue'
import Login from './components/admin/Login.vue'
import Dashboard from './components/admin/Dashboard.vue'
import CreateUser from './components/user/Create.vue'
import LoginUser from './components/user/Login.vue'
import DashboardUser from './components/user/Dashboard.vue'
import TableFilm from './components/film/Show.vue'
import AddFilm from './components/film/Add.vue'
import EditFilm from './components/film/Edit.vue'
import ShowFilm from './components/user/Film.vue'
import Riwayat from './components/user/Riwayat.vue'
import Topup from './components/admin/Topup.vue'
import Topupfield from './components/admin/Topupfield.vue'
// membuat router
const routes = [
    //route admin
    {
        name: 'create',
        path: '/api/register/admin',
        component: Create
    },
    {
        name: 'dashboard',
        path: '/api/admin/dashboard',
        component: Dashboard
    },
    {
        name: 'login',
        path: '/api/login/admin',
        component: Login
    },
    {
        name: 'topup',
        path: '/api/admin/topup',
        component: Topup
    },
    {
        name: 'topupfield',
        path: '/api/admin/topup/field',
        component: Topupfield
    },

    //route user
    {
        name: 'createUser',
        path: '/api/register/user',
        component: CreateUser
    },
    {
        name: 'dashboardUser',
        path: '/api/user/dashboard',
        component: DashboardUser
    },
    {
        name: 'loginUser',
        path: '/api/login/user',
        component: LoginUser
    },
    {
        name: 'showFilm',
        path: '/api/user/film',
        component: ShowFilm
    },
    {
        name: 'riwayat',
        path: '/api/user/riwayat',
        component: Riwayat
    },

    //film
    {
        name: 'show',
        path: '/api/admin/film/show',
        component: TableFilm
    },
    {
        name: 'add',
        path: '/api/admin/film/add',
        component: AddFilm
    },
    {
        name: 'edit',
        path: '/api/admin/film/edit',
        component: EditFilm
    }

]

Vue.prototype.$isLogin = 1

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount("#app");