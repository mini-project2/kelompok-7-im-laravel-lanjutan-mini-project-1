<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable implements JWTSubject
{
    protected $fillable = [
        'name', 'email', 'password','no_hp'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket','admin_id','id');
    }

    public function films()
    {
        return $this->hasMany('App\Film','admin_id','id');
    }

}
