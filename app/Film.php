<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = [
        'title', 'admin_id', 'price'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket','film_id','id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

}
