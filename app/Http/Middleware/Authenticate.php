<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $request->headers->set('Authorization',$request->session()->get('token'));
        $request->headers->set('Accept','application/json');
        // dd($request->header());

        if (!$request->expectsJson()) {
            return route('login_admin');
        }
    }
}
