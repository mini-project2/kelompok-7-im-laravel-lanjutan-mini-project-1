<?php

namespace App\Http\Middleware;

use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $request->headers->set('Authorization',$request->session()->get('token'));
        $request->headers->set('Accept','application/json');
        if($guard != null){
            auth()->shouldUse($guard);
        }
        return $next($request);
    }
}
