<?php

namespace App\Http\Controllers\Film;

use App\Film;
use App\Admin;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FilmController extends Controller
{
    public function __construct()
    {
      
        Auth::shouldUse('admins');
        Auth::shouldUse('users');
       
        $this->user = JWTAuth::parseToken()->authenticate();
        $this->admin = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        $film = Film::all();

        return $film->toArray();
    }

    public function getAll(){
        return view('welcome');
    }

    public function show($id)
    {
        $film = Film::where('id',$id)->get()->first();
       // $film = $this->user->films()->find($id);

        if(!$film)
        {
            return response()->json([
                'success' => false,
                'message' => 'Sorry film dengan id '.$id.' tidak ditemukan'
            ]);
        }
        
        return response()->json([
            'success' => true,
            'film' => $film
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:films,title'
        ]);
        
        $film = new Film();
        $film->title = $request->title;
        $film->price = $request->price;

        Film::create([
            'title' => $film->title,
            'admin_id' => Auth::id(),
            'price' => $film->price
        ]);
    
        return response()->json([
            'success' => true,
            'film' => $film
        ], Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $film = Film::where('id',$id)->get()->first();
       // $film = Film::find($id);
             // $film = $this->user->films()->find($id);
        $request->validate([
            'title' => 'unique:films,title'
        ]);

        $film->title = $request->title;
     
        if(!$film)
        {
            return response()->json([
                'success' => 'false',
                'message' => 'Sorry, film dengan id '.$id.' tidak ditemukan'
            ], 400);
        }

        $update = $film->save();

        if($update)
        {
            return response()->json([
                'success' => true,
                'message' => 'Film di update',
                'film' => $film
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Film gagal di update'
            ], 500);
        }
    }

    public function destroy($id)
    {
        //$film = $this->admin->films()->find($id);
        $film = Film::find($id);
        if(!$film)
        {
            return response()->json([
                'success' => false,
                'message' => 'Sorry film dengan id '.$id.' tidak ditemukan'
            ], 400);
        } 

        if($film->delete())
        {
            return response()->json([
                'success' => 'true',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Film tidak bisa dihapus'
            ], 500);
        }
    }
}
