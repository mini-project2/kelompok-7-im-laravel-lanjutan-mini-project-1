<?php

namespace App\Http\Controllers\Ticket;

use App\Film;
use App\Ticket;
use App\User;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketController extends Controller
{
    public function __construct() {

        Auth::shouldUse('admins');
        Auth::shouldUse('users');
        $this->user = JWTAuth::parseToken()->Authenticate();
    }

    public function getUserTicket()
    {
        $ticket = Ticket::where('user_id',Auth::id())->get();
        
        return $ticket->toArray();
    }

    public function store(Request $request)
    {
        $request->validate([
            'film_id' => 'required|exists:films,id',
            'ticket_date' => 'required'
        ]);

        $price_film = Film::where('id',$request->film_id)->pluck('price')->first();
        $balance = User::where('id',Auth::id())->pluck('balance')->first();
        $this->user->decrement('balance', $price_film);

        $ticket = new Ticket();
        $ticket->film_id = $request->film_id;
        $ticket->ticket_date = $request->ticket_date;
        $ticket->title = Film::where('id',$request->film_id)->pluck('title')->first();
        $ticket->price = $price_film;

        Ticket::create([
            'film_id' => $ticket->film_id,
            'ticket_date' => $ticket->ticket_date,
            'price' => $ticket->price,
            'title' => $ticket->title,
            'user_id' => Auth::id()
        ]);

        return response()->json([
            'success' => true,
            'ticket' => $ticket
        ], 200);

    }
}
