<?php

namespace App\Http\Controllers\Auth;

use App\Admin;
use App\User;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admins');
    }

    public function index(){
        return view('welcome');
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'no_hp' => ['required', 'unique:admins,no_hp'],
            'email' => ['email', 'required', 'unique:admins,email']
        ]);
        Admin::create([
            'name' => request('name'),
            'no_hp' => request('no_hp'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);
        return response()->json([
            'success' => 'true'
        ], Response::HTTP_OK);
    }

    public function login(Request $request)
    {
        $login = $request->only('no_hp','password');
      //  $jwt_token = null;

        if(!$jwt_token = auth('admins')->attempt($login))
        {
            return response()->json([
                'success' => 'true',
                'message' => 'Invalid no hp or password'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $request->session()->put('token','Bearer '.$jwt_token);

        return response()->json([
            'success' => 'true',
            'token' => $jwt_token
        ]);
    }

    public function getAdmin(Request $request)
    {
        $token = JWTAuth::getToken();

        $admin = JWTAuth::authenticate($request->token);

        return response()->json([
            'admin' => $admin
        ]);
    }

    public function logout()
    {
        Auth()->logout();

        return response()->json([
            'status' => 'success',
            'message' => 'logout'
        ], 200);
    }

    public function topUp(Request $request, $id)
    {
        $request->validate([
            'balance' => 'required'
        ]);

        $user = User::find($id);
        $user->increment('balance', $request->balance);

        return response()->json([
            'success' => true,
            'user' => $user
        ]);

    }

    public function getUser(){
        $user = User::all();

        return $user->toArray();
    }
}
