<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('users');
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function index(){
        return view('welcome');
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'no_hp' => ['required', 'unique:users,no_hp'],
            'email' => ['email', 'required', 'unique:users,email'],
            'password' => ['required']
        ]);
        User::create([
            'name' => request('name'),
            'no_hp' => request('no_hp'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);
        return response()->json([
            'success' => true
        ], Response::HTTP_OK);
    }

    public function login(Request $request)
    {
        $login = $request->only('no_hp','password');
      //  $jwt_token = null;

        if(!$jwt_token = auth('users')->attempt($login))
        {
            return response()->json([
                'success' => 'true',
                'message' => 'Invalid no hp or password'
            ], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json([
            'success' => 'true',
            'token' => $jwt_token
        ]);
    }

    public function getUser(Request $request)
    {
        $token = JWTAuth::getToken();

        $user = JWTAuth::authenticate($request->token);

        return response()->json([
            'user' => $user
        ]);
    }

    public function getUserBalance(Request $request)
    {
         $token = JWTAuth::getToken();
        // $user = JWTAuth::toUser($token);

        $user = JWTAuth::authenticate($token);
        return response()->json([
            'success' => true,
            'user' => $user->id
        ]);
    }

    public function logout()
    {
        Auth::guard('user')->logout();

        return response()->json([
            'status' => 'success',
            'message' => 'logout'
        ], 200);
    }
}
