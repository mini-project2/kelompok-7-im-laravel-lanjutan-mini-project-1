<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'user_id', 'admin_id', 'film_id',
        'ticket_date', 'price', 'title'
    ];

    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function admin()
    {
        $this->belongsTo(Admin::class);
    }

    public function film()
    {
        $this->belongsTo(Film::class);
    }
}
