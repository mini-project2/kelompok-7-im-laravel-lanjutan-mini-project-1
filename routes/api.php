<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::group([ 'middleware' => 'auth:admins', 'prefix' => 'admin' ], function ($router) {
//     Route::get('/', 'Film\FilmController@index')->name('admin_index_film');
//         Route::post('register', 'Film\FilmController@store')->name('admin_register_film');
//         Route::put('update/{id}', 'Film\FilmController@update')->name('admin_update_film');
//         Route::delete('delete/{id}', 'Film\FilmController@destroy')->name('admin_destroy_film');
//         Route::post('/{id}', 'Film\FilmController@show')->name('admin_update_film');
// });


Route::group(['prefix' => 'admin','middleware' => ['auth:admins']],function ()
{   
    Route::prefix('film')->group(function () {
        Route::get('/show','Film\FilmController@getAll');
        Route::get('/', 'Film\FilmController@index')->name('admin_index_film');
        Route::post('register', 'Film\FilmController@store')->name('admin_register_film');
        Route::put('update/{id}', 'Film\FilmController@update')->name('admin_update_film');
        Route::get('/{id}', 'Film\FilmController@show')->name('user_update_film');
        Route::delete('delete/{id}', 'Film\FilmController@destroy')->name('admin_destroy_film');
        Route::post('/{id}', 'Film\FilmController@show')->name('admin_update_film');
    });
    Route::get('dashboard','Auth\AdminController@index');
    Route::get('user','Auth\AdminController@getUser');
    Route::post('topup/{id}', 'Auth\AdminController@topUp')->name('admin_topup');

    Route::get('info', 'Auth\AdminController@getAdmin')->name('admin_info');
    Route::post('logout', 'Auth\AdminController@Logout')->name('admin_logout');
});


Route::group(['prefix' => 'user','middleware' => ['auth:users']],function ()
{   
    Route::prefix('film')->group(function () {
        Route::get('/', 'Film\FilmController@index')->name('user_index_film');
        Route::post('/{id}', 'Film\FilmController@show')->name('user_update_film');
    });

    Route::get('dashboard','Auth\UserController@index');
    Route::prefix('ticket')->group(function () {
        Route::get('/', 'Ticket\TicketController@getUserTicket')->name('user_index_ticket');
        Route::post('/buy', 'Ticket\TicketController@store')->name('user_update_ticket');
    });

    Route::get('balance', 'Auth\UserController@getUserBalance')->name('user_balance');
    Route::get('info', 'Auth\UserController@getUser')->name('user_info');
    Route::post('logout', 'Auth\UserController@logout')->name('user_logout');
});

Route::prefix('register')->group(function () {
    Route::get('admin','Auth\AdminController@index');
    Route::post('admin', 'Auth\AdminController@register')->name('admin_register');
    Route::get('user','Auth\UserController@index');
    Route::post('user', 'Auth\UserController@register')->name('user_register');
});

Route::prefix('login')->group(function () {
    Route::get('admin','Auth\AdminController@index')->name('login_admin');
    Route::post('admin', 'Auth\AdminController@login')->name('admin_login');
    Route::get('user','Auth\UserController@index')->name('login_user');
    Route::post('user', 'Auth\UserController@login')->name('user_login');
});
